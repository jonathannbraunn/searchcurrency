Wir werden während und nach der Bereitstellung unseres Programms Tests durchführen.

### Das Testen unserer API wird mit **Jestjs** durchgeführt

```javascript
test('API returns EUR', async () => {
  const response = await fetch('api.link/currency/germany');
  const data = await response.json(); 

  expect(data.currency).toBe('EUR');
});
```
Im obigen Fall testen wir, ob die API korrekt aufgerufen wird und das zurückgibt, was wir von ihr erwarten


### Das Testen unserer Benutzeroberfläche wird mit **Selenium** durchgeführt

```python
driver.get("http://www.example.com")

search_field = driver.find_element_by_id("search-field")
search_field.send_keys("Germany")

label = driver.wait.until(EC.presence_of_element_located((By.ID, "currency-label")))

label_text = label.text
if label_text == "EUR":
  print("Test passed: Label text is EUR")
else:
  print("Test failed: Label text is not EUR")

driver.close()
```
Hier testen wir, dass Sie nach Eingabe von "Deutschland" tatsächlich die erwartete Währung zurückerhalten.

Mit Selenium können wir alle verschiedenen Funktionen testen, die wir benötigen, damit unsere App bereitgestellt werden kann.
