 **Testfälle**
-------------
# **Prüfung des Land Eingabefeldes** 

### **Vorbedingungen von Tests:**
- Die anwendung muss online und erreichbar sein.
- Das Suchfeld wird korrekt wiedergegeben
- Ein gültiges Land wird in das Feld eingegeben

### **Objekte:**
- Länder Sucheingabe
- Länderergebnis

### **Eingabedaten:**
- "Germany"

### **Handlung:**
Die Suche wird nach eingabe vom Land automatisch ausgeführt  

### **Erwartete Ergebnisse:**
Bei dem Länderergebnis wird nach eingabe vom Land die Währung angezeigt werden. <br/>
Bei "Germany" würde z. B. als Ergebnis "EUR" erwartet.

### **Erwartete Nachbedingungen:**
- Auf der Website wird ein neues Feld mit dem Ergebnis eingeblendet
- Der Test prüft ob das Ergebnis zum Land passt.
- Es wird geprüft ob die Suche akzeptablen Zeitraum abgeschlossen wird


# **Testen der Währungswertsuche**

### **Vorbedingungen von Tests:**
- Die anwendung muss online und erreichbar sein.
- Die Länder-Eingabefelder werden korrekt wiedergegeben
- Gültige Länder werden in die Eingabefelder eingegeben.

### **Objekte**
- Erstes Land-Eingabefeld
- Zweites Länder-Eingabefeld
- Ergebnis Container

### **Eingabedaten**
- "EUR"
- "USD"

### **Handlung**
Die Suche wird nach eingabe vom den Ländern automatisch ausgeführt

### **Erwartete Ergebnisse:**
In dem Ergebnis Kontainer wird angezeigt wie die Währungen im Vergleich zueinander stehen.


### **Erwartete Nachbedingungen:**
- Auf der Website gibt es ein neues Containerelement, das das Ergebnis der Konvertierung anzeigt
- Der Test prüft mit regex, ob das Ergebnis in der erwarteten Form vorliegt.
- Es wird geprüft ob die Suche akzeptablen Zeitraum abgeschlossen wird 
<br/><br/>

# **Testen ob das Liniendiagramm erstellt wurde**

### **Vorbedingungen von Tests:**
- Die anwendung muss online und erreichbar sein.
- Die Länder-Eingabefelder werden korrekt wiedergegeben
- Gültige Länder werden in die Eingabefelder eingegeben.
- Währungswertsuche ist voll funktionsfähig

### **Objekte**
- Erstes Land-Eingabefeld
- Zweites Länder-Eingabefeld
- Datumspicker
- Liniendiagramm

### **Eingabedaten**
- "EUR"
- "USD"
- Datum, an dem der Benutzer das Liniendiagramm beginnen möchte
- Datum, bis zu dem das Liniendiagramm laufen soll

### **Handlung**
Nachdem die beiden Währungen eingegeben und die Daten ausgewählt wurden, beginnt der Prozess.

### **Erwartete Ergebnisse:**
Es wird ein Liniendiagramm erstellt, das von einem Datum zum anderen verläuft

### **Erwartete Nachbedingungen:**
- Daten werden in der Datumsauswahl im richtigen Format angezeigt
- Der Zeilenchat wird angezeigtDer Zeilenchat wird angezeigt


# **Test der Benutzerregistrierung**
### **Vorbedingungen von Tests:**
- Die Anwendung muss online und erreichbar sein.
- Das Anmeldeformular wird korrekt wiedergegeben

### **Objekte**
- Email Eingabefeld
- Benutzername Eingabefeld
- Benutzerpasswort Eingabefeld


### **Eingabedaten**
- E-Mail, die die E-Mail-Prüfung besteht
- Passwort mit mehr als 8 Zeichen
- Eindeutiger Benutzername
 
### **Handlung**
Nachder erfolgreichen Validierung der einzelnen Felder wird der Benutzer nach dem Absenden generiert

### **Erwartete Ergebnisse:**

Nach erfolgreicher Erstellung des Benutzerkontos wird der Benutzer auf die Hauptwebseite weitergeleitet

### **Erwartete Nachbedingungen:**
- Benutzer ist erstellt worden und wird in der Datenbank angezeigt
- Einloggen mit dem neuem Konto funktioniert


# **Test der Benutzeranmeldung**
### **Vorbedingungen von Tests:**
- Die Anwendung muss online und erreichbar sein.
- Die Benutzer müssen ein eigenes Konto besitzen.

### **Objekte**
- Feld für Benutzername oder E-mail 
- Feld für Passwort

### **Eingabedaten**
- "Max33"
- "ABC321"

### **Handlung**
- Der Benutzer gibt seine E-mail oder seinen Nutzernamen ein.
- Der Benutzer gibt sein Passwort ein.

### **Erwartete Ergebnisse:**
- Der Benutzer hat seine Daten richtig eingegeben und wird auf sein Konto weitergeleitet.
- Der Benutzer kriegt eine Fehlermeldung, weil seine Anmeldedaten nicht stimmen.

### **Erwartete Nachbedingungen:**
- Die Hauptseite wird angezeigt, nachdem sich der Nutzer angemeldet hat.
- Der Benutzer erhält eine Fehlermeldung, wenn er versucht, sich mit ungültigen Anmeldeinformationen anzumelden
- Der Benutzer kann sich erfolgreich abmelden und danach keinen Zugriff mehr auf geschützte Bereiche haben.

# **Test vom Admin Panel**

### **Vorbedingungen von Tests:**
- Ein Konto kann den Admin-Status erhalten
- Der Admin soll sich einloggen können

### **Objekte**
- Feld für Admin Konfiguration

### **Eingabedaten**
- "/delete User Max33"

### **Handlung**
- Der Admin trägt den Befehl ein, um den Nutzer Max33 zu löschen.

### **Erwartete Ergebnisse:**
- Das Konto von Max33 ist nicht mehr vorhanden.

### **Erwartete Nachbedingungen:**
- Der Nutzer bekommt beim Eingeben seiner Kenndaten eine Meldung: "Dieses Konto existiert nicht oder wurde entfernt"


# **Test des DatePicker**

### **Vorbedingungen von Tests:**
- Die anwendung muss online und erreichbar sein.
- DatePicker muss funktionell sein.
### **Objekte**
- Start Datum DatePicker
- End Datum DatePicker
### **Eingabedaten**
- Start Datum
- End Datum
### **Handlung**
- Der user muss das Start Datum und das End Datum auswählen.
### **Erwartete Ergebnisse:**
- Ein Liniendiagramm wird erstellt mit dem ausgewählten Datum.
### **Erwartete Nachbedingungen:**
- Es gibt ein Liniendiagramm mit ausgewähltem Zeitraum.
