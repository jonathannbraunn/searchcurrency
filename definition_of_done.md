**Definition of Done – DoD
Currency Converter**


- Die benötigte Benutzerdokumentation wurde erstellt und ist verfügbar.
- Alle funktionalen Testfälle wurden durchgeführt und erfolgreich beendet.
- Alle Akzeptanzkriterien sind erfüllt.
- Es sind keine kritischen Bugs offen.
